# Build project

## Create a virtual env

```bash
python3 -m venv venv
```

## Start the virtual env

```bash
source venv/bin/activate
```

### Install requirements

```bash
pip install -r requirements.txt
```

## Run script

```bash
python run.py
```





# Run this only when you close the project

## Deactivate venv (when you finish)

```bash
deactivate
```


## Generate archive (not needed)

```bash
cd ../ && zip -r python-images.zip python-images/ && cd python-images && mv ../python-images.zip .
```
